<!DOCTYPE HTML>
<!--
	
-->
<html>
	<head>
		<title>Sagebrush White Paper</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		
			<div id="wrapper" class="fade-in">

				
					<div id="intro">
						<h1>This is<br />
						Sagebrush</h1>
						<p>The largest online art gallery in the crypto world.
							You will be able to market NFT, NETART, DIGITAL ART, PAINTINGS, Sculptures, Drawings, Photography, Engravings and Collage.</p>
						<ul class="actions">
							<li><a href="#header" class="button icon solid solo fa-arrow-down scrolly">Continue</a></li>
						</ul>
					</div>

				
					<header id="header">
						<a href="index.html" class="logo">Sagebrush</a>
					</header>

				
					<nav id="nav">
						<ul class="links">
							
							
						</ul>
						<ul class="icons">
							
							<li><a href="https://twitter.com/BscSagebrush" target="_blank" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							
							<li><a href="https://www.instagram.com/sagebrushbsc" target="_blank" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="https://t.me/Sagebrushproject" target="_blank" class="icon brands fa-telegram"><span class="label">Telegram</span></a></li>
						</ul>
					</nav>

				
					<div id="main">

						
							<article class="post featured">
								<header class="major">
									<span class="date">Sagebrush Project </span>
									<h3><a href="#">This project is created by and for Artists<br /><br />
									You don't have to "farm" or lock your funds at any time.</a></h3>
									<p>By simply keeping the token in your wallet, you will receive commissions for being part of the community.</p>
								</header>
								<a href="#" class="image main"><img src="../assets/img/w/sg2.png" alt="" /></a>
								
							</article>

						
							<section class="posts">
								<article>
									<header>
										
										<h2><a href="#">Reflections</a></h2>
										
									</header>
									<a href="#" class="image fit"><img src="../assets/img/w/sg5.png" alt="" /></a>
									<p>The longer you keep the token in your wallet, the more rewards you receive naturally. You will be receiving your proportional share of 2% of each transaction. When transaction volumes are high, the percentage of commission that each crew member receives will increase. We firmly believe that is the fairest and most stable system to provide a successful investment!</p>
									
								</article>
								<article>
									<header>
										
										<h2><a href="#">Token Burn</a></h2>
									</header>
									<a href="#" class="image fit"><img src="../assets/img/w/sg1.png" alt="" /></a>
									<p>After launch, approximately 50% of the total is sent to the Black Hole address permanently decreasing the number of tokens in circulation. Black Hole address: 0x000000000000000000000000000000000000dead Since Black Hole is also classified as a token holder, you will receive your proportional part just like all the others, constantly and forever reducing the number of tokens in circulation.</p>
									<ul class="actions special">
										
									</ul>
								</article>
								<article>
									<header>
										
										<h2><a href="#">Liquidity Funds</a></h2>
									</header>
									<a href="#" class="image fit"><img src="../assets/img/w/sg3.png" alt="" /></a>
									<p>Liquidity funds are constantly growing as 3% of each transaction is transformed into LP tokens. In this way, the number of tokens in circulation is reduced by increasing the base price.</p>
									<ul class="actions special">
										
									</ul>
								</article>
								<article>
									<header>
										
										<h2><a href="#">Safety</a></h2>
									</header>
									<a href="#" class="image fit"><img src="../assets/img/w/sg4.png" alt="" /></a>
									<p>We are working with the TechRate team to audit the project.</p>
									<ul class="actions special">
										
									</ul>
								</article>
								<article>
									<header>
										<p>Soon we will be adding more info ...</p>
									</header>
								
								</article>
								<article>
									<header>
										
									</header>
									
								</article>
							</section>

						


					</div>

				
					<div id="copyright">
						<ul><li>&copy; Sagebrush Project </a></li></ul>
					</div>

			</div>

		
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>