<!DOCTYPE html>
 <?require("info.php"); ?>
<html lang="en">
<head>
  <title>Sagebrush</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Sagebrush Project">
  <meta name="keywords" content="Sagebrush Project, Binance Smart Chain">
  <meta name="author" content="SageBrush - 2021">
  <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

<!-- notification for small viewports and landscape oriented smartphones -->
<div class="device-notification">
  <a class="device-notification--logo" href="#0">
    <img src="assets/img/logo.png" alt="Sagebrush Project">
    <p>Sagebrush</p>
  </a>
  <p class="device-notification--message">Sagebrush Project has so much to offer that you won't be disappointed.</p>
</div>

<div class="perspective effect-rotate-left">
  <div class="container"><div class="outer-nav--return"></div>
    <div id="viewport" class="l-viewport">
      <div class="l-wrapper">
        <header class="header">
          <a class="header--logo" href="#0">
		  <p>Sagebrush Project</p>
            <img src="assets/img/logo.png" width="8%" alt="Sagebrush Project">
            
          </a>
         
          <div class="header--nav-toggle">
            <span></span>
          </div>
        </header>
        <nav class="l-side-nav">
          <ul class="side-nav">
            <li class="is-active"><span>Home</span></li>
           
			<li><span>Road Map</span></li>
            <li><span>White Paper</span></li>
            <li><span>Social</span></li>
            <li><span>Tools</span></li>
          </ul>
        </nav>
        <ul class="l-main-content main-content">
          <li class="l-section section section--is-active">
            <div class="intro">
              <div class="intro--banner">
			  <h1>Your next<br>interactive<br>TOKEN</h1>
                <h3>
				
				T. Supply: <label class="valueToken2"></label></Br>
				Value: <label class="valueToken"></label></Br>
				Liquidity (USD): <label class="valueName"></label></Br>
				Burned: <label class="valueSymbol"></label>
				
				
				</h3>
               
                   
                  
                
                <img src="assets/img/introduction-visual.png" alt="Welcome">
              </div>
              <div class="intro--options">
                
                 <center>
                  <p><a href="https://v1exchange.pancakeswap.finance/#/swap?outputCurrency=<? echo $ContratoP;?>" target="_blank"><img src="assets/img/Buy2.png" alt="Buy" width="50%" ></a></p>
                </center>
				
				 
				
                <a href="#0">
				
				
                  <h3>Automatic LP</h3>
                  <p>Burns 5% of each transaction.  2% will be burned eternally.
    3% will be redistributed to all holders.</p>
                </a>
				
				

                <a href="#0">
                  <h3>Static Rewards</h3>
                  <p>Holders obtain passive rewards through transactions.</p>
                </a>
              </div>
            </div>
          </li>
          <li class="l-section section">
            <div class="work">
              <h2>ROAD MAP</h2>
              <div class="work--lockup">
                <ul class="slider">
                  <li class="slider--item slider--item-left">
                    <a href="#0">
                      <div class="slider--item-image">
                        <img src="assets/img/work-victory.jpg" alt="Victory">
                      </div>
                      <p class="slider--item-title">STAGE.3</p>
                      <p class="slider--item-description">We will create the largest online art gallery in the world of crypto. you will be able to market NFT, NETART, DIGITAL ART, PAINTINGS, Sculptures, Drawings, Photography, Engravings and Collage</p>
                    </a>
                  </li>
                  <li class="slider--item slider--item-center">
                    <a href="#0">
                      <div class="slider--item-image">
                        <img src="assets/img/1.gif" alt="Metiew and Smith">
                      </div>
                      <p class="slider--item-title">STAGE.1</p>
                      <p class="slider--item-description">Website, Social Networks, Launch the token, giving us the time necessary to make everything transparentand fair.</p>
                    </a>
                  </li>
                  <li class="slider--item slider--item-right">
                    <a href="#0">
                      <div class="slider--item-image">
                        <img src="assets/img/work-alex-nowak.jpg" alt="Alex Nowak">
                      </div>
                      <p class="slider--item-title">STAGE.2</p>
                      <p class="slider--item-description">Listing on Coingecko and Coinmarketcap, Smart contract audit, Marketing and further partnerships.</p>
                    </a>
                  </li>
                </ul>
                <div class="slider--prev">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                  <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                    <path d="M561,1169C525,1155,10,640,3,612c-3-13,1-36,8-52c8-15,134-145,281-289C527,41,562,10,590,10c22,0,41,9,61,29
                    c55,55,49,64-163,278L296,510h575c564,0,576,0,597,20c46,43,37,109-18,137c-19,10-159,13-590,13l-565,1l182,180
                    c101,99,187,188,193,199c16,30,12,57-12,84C631,1174,595,1183,561,1169z"/>
                  </g>
                  </svg>
                </div>
                <div class="slider--next">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                  <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                    <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                  </g>
                  </svg>
                </div>
              </div>
            </div>
          </li>
          <li class="l-section section">
            <div class="about">
              <div class="about--banner">
                <h2>We<br>believe in<br>dreamy<br>people</h2>
                <a href="https://sagebrush.space/whitepaper/" target="_blank">WHITE PAPER 
                  <span>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                    <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                      <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                    </g>
                    </svg>
                  </span>
                </a>
                <img src="assets/img/about-visual.png" alt="">
              </div>
             
            </div>
          </li>
          <li class="l-section section">
            <div class="contact">
              <div class="contact--lockup">
                <div class="modal">
                  <div class="modal--information">
                    <p>Follow us on our networks</p>
                    
                  </div>
                  <ul class="modal--options">
                    <li><a href="https://twitter.com/BscSagebrush" target="_blank">TWITTER</a></li>
                    <li><a href="https://www.instagram.com/sagebrushbsc" target="_blank">INSTAGRAM</a></li>
					 <li><a href="https://t.me/Sagebrushproject" target="_blank">Telegram</a></li>
                     </ul>
                </div>
              </div>
            </div>
          </li>
          <li class="l-section section">
            <div class="hire">
				
				<div class="intro--options">
				<a href="https://v1exchange.pancakeswap.finance/#/swap?outputCurrency=<? echo $ContratoP;?>" target="_blank"><center><img src="assets/img/t1.png" alt="Buy" width="20%" ></center></Br><center><img src="assets/img/Buy2.png" alt="Buy" width="40%" ></center></a>
				<a href="https://poocoin.app/tokens/<? echo $ContratoP;?>" target="_blank"><center><img src="assets/img/t2.png" alt="Buy" width="20%" ></center></Br><center><img src="assets/img/Chart2.png" alt="Buy" width="40%" ></center></a>
				<a href="https://bscscan.com/token/<? echo $ContratoP;?>" target="_blank"><center><img src="assets/img/t3.png" alt="Buy" width="20%" ></center></Br><center><img src="assets/img/Bsc2.png" alt="Buy" width="40%" ></center></a>
			  
			  </div>
             
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <ul class="outer-nav">
    <li class="is-active">Home</li>
	
    <li>Road Map</li>
    <li>White Paper</li>
    <li>Social</li>
    <li>Tools</li>
  </ul>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-2.2.4.min.js"><\/script>')</script>
<script src="assets/js/functions-min.js"></script>







<script src="https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


<script>
  $(document).ready(async function() {
    let pancake   = await $.get("https://api.pancakeswap.info/api/tokens");

    let sfm_data  = pancake['data']["0x706954491EBFD6b38e5B1e6F079dBEfae0F5EBa0"];
    let bnb_data  = pancake['data']["0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c"];

    let sfm_price = parseFloat(sfm_data['price']).toFixed(9);
    let bnb_price = parseFloat(bnb_data['price']).toFixed(9);

    let liquidity = await getLiquidity(sfm_price, bnb_price);
    let burned    = await getBurned();
	let supply = await getSupply(burned);
    let remaining = 1_000_000_000_000_000 - burned;
    let mcap      = (remaining * sfm_price).toFixed(2);

    $('.valueToken').html(sfm_price);
    $('.valueName').html(insertCommas(parseFloat(liquidity).toFixed(2)));
    $('.valueSymbol').html((burned / 1_000_000_000_000).toFixed(3)+" <small>T</small>");
    $('.valueDecimals').html(insertCommas(mcap));
	 $('.valueToken2').html(supply);
});

async function getPrice() {
    let token = "0x706954491EBFD6b38e5B1e6F079dBEfae0F5EBa0";
    let data  = await getPriceForToken(token);
    return parseFloat(data['price']).toFixed(9);
}

async function getPriceForToken(address) {
    let pancake = await $.get("https://api.pancakeswap.info/api/tokens");
    let token   = pancake['data'][address];
    return parseFloat(token['price']).toFixed(9);
}

async function getLiquidity(sfm_price, bnb_price) {
    let bnb_address = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
    let pck_wallet  = "0x9adc6fb78cefa07e13e9294f150c1e8c1dd566c0";
    let sfm_address = "0x706954491ebfd6b38e5b1e6f079dbefae0f5eba0";

    let bnb = await getBalance(bnb_address, pck_wallet);
    let sfm = await getBalance(sfm_address, pck_wallet);

    return ((bnb * bnb_price) + (sfm * sfm_price));
}

async function getBurned() {
    let address = "0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3"; // contract address
    let wallet  = "0x0000000000000000000000000000000000000001"; // burn address
    let result  = await getBalance(address, wallet);
    return result;
}

//"https://bsc-dataseed1.binance.org:443"
async function getBalance(fromWallet, toWallet) {
    let web3 = new Web3("https://bsc-dataseed1.binance.org:443");

    var contract = new web3.eth.Contract(getContract(), fromWallet);
    var balance  = await contract.methods.balanceOf(toWallet).call();
    var decimals = await contract.methods.decimals().call();

    let formatted = balance / 10 ** decimals;
    return formatted;
}


async function getSupply(burned) {
    let web3 = new Web3("https://bsc-dataseed1.binance.org:443");

    let address = "0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3"; // contract address

    var contract = new web3.eth.Contract(getContract(), address);
    var supply  = await contract.methods.totalSupply().call();
    var decimals = await contract.methods.decimals().call() * -1;

    const circ = (supply  / 10 ** 9) - (burned );

    return  circ;

}

function insertCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getContract() {
    return [
        {
            "inputs": [],
            "stateMutability": "nonpayable",
            "type": "constructor"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "owner",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "spender",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "value",
                    "type": "uint256"
                }
            ],
            "name": "Approval",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "minTokensBeforeSwap",
                    "type": "uint256"
                }
            ],
            "name": "MinTokensBeforeSwapUpdated",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "previousOwner",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "newOwner",
                    "type": "address"
                }
            ],
            "name": "OwnershipTransferred",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "tokensSwapped",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "ethReceived",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "tokensIntoLiqudity",
                    "type": "uint256"
                }
            ],
            "name": "SwapAndLiquify",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "internalType": "bool",
                    "name": "enabled",
                    "type": "bool"
                }
            ],
            "name": "SwapAndLiquifyEnabledUpdated",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "from",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "to",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "value",
                    "type": "uint256"
                }
            ],
            "name": "Transfer",
            "type": "event"
        },
        {
            "inputs": [],
            "name": "_liquidityFee",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "_maxTxAmount",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "_taxFee",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "owner",
                    "type": "address"
                },
                {
                    "internalType": "address",
                    "name": "spender",
                    "type": "address"
                }
            ],
            "name": "allowance",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "spender",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "amount",
                    "type": "uint256"
                }
            ],
            "name": "approve",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "balanceOf",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "decimals",
            "outputs": [
                {
                    "internalType": "uint8",
                    "name": "",
                    "type": "uint8"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "spender",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "subtractedValue",
                    "type": "uint256"
                }
            ],
            "name": "decreaseAllowance",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "tAmount",
                    "type": "uint256"
                }
            ],
            "name": "deliver",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "excludeFromFee",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "excludeFromReward",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "geUnlockTime",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "includeInFee",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "includeInReward",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "spender",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "addedValue",
                    "type": "uint256"
                }
            ],
            "name": "increaseAllowance",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "isExcludedFromFee",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "account",
                    "type": "address"
                }
            ],
            "name": "isExcludedFromReward",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "time",
                    "type": "uint256"
                }
            ],
            "name": "lock",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "name",
            "outputs": [
                {
                    "internalType": "string",
                    "name": "",
                    "type": "string"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "owner",
            "outputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "tAmount",
                    "type": "uint256"
                },
                {
                    "internalType": "bool",
                    "name": "deductTransferFee",
                    "type": "bool"
                }
            ],
            "name": "reflectionFromToken",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "renounceOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "liquidityFee",
                    "type": "uint256"
                }
            ],
            "name": "setLiquidityFeePercent",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "maxTxPercent",
                    "type": "uint256"
                }
            ],
            "name": "setMaxTxPercent",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "bool",
                    "name": "_enabled",
                    "type": "bool"
                }
            ],
            "name": "setSwapAndLiquifyEnabled",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "taxFee",
                    "type": "uint256"
                }
            ],
            "name": "setTaxFeePercent",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "swapAndLiquifyEnabled",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "symbol",
            "outputs": [
                {
                    "internalType": "string",
                    "name": "",
                    "type": "string"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "rAmount",
                    "type": "uint256"
                }
            ],
            "name": "tokenFromReflection",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "totalFees",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "totalSupply",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "recipient",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "amount",
                    "type": "uint256"
                }
            ],
            "name": "transfer",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "sender",
                    "type": "address"
                },
                {
                    "internalType": "address",
                    "name": "recipient",
                    "type": "address"
                },
                {
                    "internalType": "uint256",
                    "name": "amount",
                    "type": "uint256"
                }
            ],
            "name": "transferFrom",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs":[],
            "name":"latestRoundData",
            "outputs":[
               {
                  "internalType":"uint80",
                  "name":"roundId",
                  "type":"uint80"
               },
               {
                  "internalType":"int256",
                  "name":"answer",
                  "type":"int256"
               },
               {
                  "internalType":"uint256",
                  "name":"startedAt",
                  "type":"uint256"
               },
               {
                  "internalType":"uint256",
                  "name":"updatedAt",
                  "type":"uint256"
               },
               {
                  "internalType":"uint80",
                  "name":"answeredInRound",
                  "type":"uint80"
               }
            ],
            "stateMutability":"view",
            "type":"function"
        },
        {
            "inputs": [
                {
                    "internalType": "address",
                    "name": "newOwner",
                    "type": "address"
                }
            ],
            "name": "transferOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "uniswapV2Pair",
            "outputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "uniswapV2Router",
            "outputs": [
                {
                    "internalType": "contract IUniswapV2Router02",
                    "name": "",
                    "type": "address"
                }
            ],
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "name": "unlock",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "stateMutability": "payable",
            "type": "receive"
        }
    ];
}
</script>











</body>
</html>
